-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 02-02-2023 a las 22:19:30
-- Versión del servidor: 5.7.36
-- Versión de PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `brokers`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `broker`
--

DROP TABLE IF EXISTS `broker`;
CREATE TABLE IF NOT EXISTS `broker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `broker`
--

INSERT INTO `broker` (`id`, `name`) VALUES
(1, 'Broker-One');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name_es` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `category_name_en` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `category_active` tinyint(1) DEFAULT NULL,
  `category_deductible` decimal(10,2) DEFAULT NULL,
  `category_autorization` decimal(10,2) DEFAULT NULL,
  `category_code` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `category_order` varchar(45) COLLATE utf8_spanish_ci DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`category_id`, `category_name_es`, `category_name_en`, `category_active`, `category_deductible`, `category_autorization`, `category_code`, `category_order`) VALUES
(1, 'Mini', 'Mini', 1, NULL, NULL, 'M', '99'),
(3, 'Económico', 'Economy', 1, NULL, NULL, 'E', '98'),
(7, 'Intermedio', 'Intermediate', 1, NULL, NULL, 'I', '96'),
(9, 'Estándar', 'Standard', 1, NULL, NULL, 'S', '95'),
(11, 'Fullsize', 'Fullsize', 1, NULL, NULL, 'F', '94'),
(13, 'Premium', 'Premium', 1, NULL, NULL, 'P', '93'),
(15, 'De Lujo', 'Luxury', 1, NULL, NULL, 'L', '92'),
(17, 'Gran Tamaño', 'Oversize', 1, NULL, NULL, 'O', '91'),
(18, 'Especial', 'Special', 1, NULL, NULL, 'X', '90'),
(19, 'Compacto', 'Compact', 1, NULL, NULL, 'C', '97');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `channels`
--

DROP TABLE IF EXISTS `channels`;
CREATE TABLE IF NOT EXISTS `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `prefix` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `setting_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(35) NOT NULL DEFAULT '',
  `countryCode` char(3) NOT NULL DEFAULT '',
  `district` char(20) NOT NULL DEFAULT '',
  `provider` int(11) NOT NULL DEFAULT '0',
  `iata` varchar(5) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `price_from_mxn` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '0',
  `price_from_usd` decimal(10,2) NOT NULL DEFAULT '0.00',
  `url_city_web_es` varchar(50) DEFAULT NULL,
  `url_city_web_en` varchar(50) DEFAULT NULL,
  `url_city_es` varchar(50) DEFAULT NULL,
  `url_city_en` varchar(50) DEFAULT NULL,
  `region_iso3166_2` varchar(50) DEFAULT NULL,
  `city_es` varchar(50) DEFAULT NULL,
  `city_en` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `countryCode` (`countryCode`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `city`
--

INSERT INTO `city` (`id`, `name`, `countryCode`, `district`, `provider`, `iata`, `url`, `price_from_mxn`, `price_from_usd`, `url_city_web_es`, `url_city_web_en`, `url_city_es`, `url_city_en`, `region_iso3166_2`, `city_es`, `city_en`) VALUES
(1, 'Kabul', 'AFG', 'Kabol', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Qandahar', 'AFG', 'Qandahar', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Herat', 'AFG', 'Herat', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Mazar-e-Sharif', 'AFG', 'Balkh', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Amsterdam', 'NLD', 'Noord-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Rotterdam', 'NLD', 'Zuid-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Haag', 'NLD', 'Zuid-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Utrecht', 'NLD', 'Utrecht', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'Eindhoven', 'NLD', 'Noord-Brabant', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Tilburg', 'NLD', 'Noord-Brabant', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Groningen', 'NLD', 'Groningen', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Breda', 'NLD', 'Noord-Brabant', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'Apeldoorn', 'NLD', 'Gelderland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Nijmegen', 'NLD', 'Gelderland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Enschede', 'NLD', 'Overijssel', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Haarlem', 'NLD', 'Noord-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Almere', 'NLD', 'Flevoland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'Arnhem', 'NLD', 'Gelderland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'Zaanstad', 'NLD', 'Noord-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '´s-Hertogenbosch', 'NLD', 'Noord-Brabant', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'Amersfoort', 'NLD', 'Utrecht', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'Maastricht', 'NLD', 'Limburg', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'Dordrecht', 'NLD', 'Zuid-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'Leiden', 'NLD', 'Zuid-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'Haarlemmermeer', 'NLD', 'Noord-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'Zoetermeer', 'NLD', 'Zuid-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'Emmen', 'NLD', 'Drenthe', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'Zwolle', 'NLD', 'Overijssel', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'Ede', 'NLD', 'Gelderland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'Delft', 'NLD', 'Zuid-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'Heerlen', 'NLD', 'Limburg', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'Alkmaar', 'NLD', 'Noord-Holland', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'Willemstad', 'ANT', 'Curaçao', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'Tirana', 'ALB', 'Tirana', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 'Alger', 'DZA', 'Alger', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 'Oran', 'DZA', 'Oran', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'Constantine', 'DZA', 'Constantine', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'Annaba', 'DZA', 'Annaba', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'Batna', 'DZA', 'Batna', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'Sétif', 'DZA', 'Sétif', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'Sidi Bel Abbès', 'DZA', 'Sidi Bel Abbès', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'Skikda', 'DZA', 'Skikda', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'Biskra', 'DZA', 'Biskra', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'Blida (el-Boulaida)', 'DZA', 'Blida', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'Béjaïa', 'DZA', 'Béjaïa', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'Mostaganem', 'DZA', 'Mostaganem', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Tébessa', 'DZA', 'Tébessa', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Tlemcen (Tilimsen)', 'DZA', 'Tlemcen', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Béchar', 'DZA', 'Béchar', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Tiaret', 'DZA', 'Tiaret', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'Ech-Chleff (el-Asnam)', 'DZA', 'Chlef', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'Ghardaïa', 'DZA', 'Ghardaïa', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'Tafuna', 'ASM', 'Tutuila', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'Fagatogo', 'ASM', 'Tutuila', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Andorra la Vella', 'AND', 'Andorra la Vella', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'Luanda', 'AGO', 'Luanda', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Huambo', 'AGO', 'Huambo', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Lobito', 'AGO', 'Benguela', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Benguela', 'AGO', 'Benguela', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Namibe', 'AGO', 'Namibe', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'South Hill', 'AIA', '?', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'The Valley', 'AIA', '?', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'Saint John´s', 'ATG', 'St John', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'Dubai', 'ARE', 'Dubai', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Abu Dhabi', 'ARE', 'Abu Dhabi', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'Sharja', 'ARE', 'Sharja', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'al-Ayn', 'ARE', 'Abu Dhabi', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'Ajman', 'ARE', 'Ajman', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'Buenos Aires', 'ARG', 'Distrito Federal', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'La Matanza', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'Córdoba', 'ARG', 'Córdoba', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Rosario', 'ARG', 'Santa Fé', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'Lomas de Zamora', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 'Quilmes', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 'Almirante Brown', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 'La Plata', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 'Mar del Plata', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 'San Miguel de Tucumán', 'ARG', 'Tucumán', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 'Lanús', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 'Merlo', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 'General San Martín', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 'Salta', 'ARG', 'Salta', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 'Moreno', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 'Santa Fé', 'ARG', 'Santa Fé', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 'Avellaneda', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 'Tres de Febrero', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 'Morón', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 'Florencio Varela', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 'San Isidro', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 'Tigre', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, 'Malvinas Argentinas', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 'Vicente López', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 'Berazategui', 'ARG', 'Buenos Aires', 0, NULL, NULL, '0.00', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(3) COLLATE utf8mb4_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fleet`
--

DROP TABLE IF EXISTS `fleet`;
CREATE TABLE IF NOT EXISTS `fleet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acriss` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_spanish_ci NOT NULL,
  `doors` tinyint(2) NOT NULL,
  `passangers` tinyint(2) NOT NULL,
  `luggages` tinyint(2) NOT NULL,
  `category_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `transmission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fleet_locations`
--

DROP TABLE IF EXISTS `fleet_locations`;
CREATE TABLE IF NOT EXISTS `fleet_locations` (
  `fleet_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  KEY `fleet_id` (`fleet_id`),
  KEY `location_id` (`location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `location`
--

DROP TABLE IF EXISTS `location`;
CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iata` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_spanish_ci NOT NULL,
  `address_1` varchar(150) COLLATE utf8mb4_spanish_ci NOT NULL,
  `address_2` varchar(150) COLLATE utf8mb4_spanish_ci NOT NULL,
  `is24hours` tinyint(1) NOT NULL,
  `open_schedule` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `close_schedule` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `latitude` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `longitude` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cp` varchar(5) COLLATE utf8mb4_spanish_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `broker_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`),
  KEY `broker_id` (`broker_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `location`
--

INSERT INTO `location` (`id`, `iata`, `name`, `address_1`, `address_2`, `is24hours`, `open_schedule`, `close_schedule`, `latitude`, `longitude`, `cp`, `city_id`, `provider_id`, `broker_id`) VALUES
(1, 'CUN', 'Aeropuerto Internacional de Cancún', 'Cancun - Chetumal Km 22', 'Cancún, Q.R', 1, 'NO', 'NO', '21°02′12″N ', '86°52′37″O', '77565', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rates`
--

DROP TABLE IF EXISTS `rates`;
CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `setting_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `_schema` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `summary`
--

DROP TABLE IF EXISTS `summary`;
CREATE TABLE IF NOT EXISTS `summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8mb4_spanish_ci NOT NULL,
  `password` varchar(128) COLLATE utf8mb4_spanish_ci NOT NULL,
  `email` varchar(128) COLLATE utf8mb4_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `email`) VALUES
(1, 'Alan', 'pass1', 'testing@example.com'),
(2, 'test2', 'pass2', 'test2@example.com'),
(3, 'test3', 'pass3', 'test3@example.com'),
(4, 'test4', 'pass4', 'test4@example.com'),
(5, 'test5', 'pass5', 'test5@example.com'),
(6, 'test6', 'pass6', 'test6@example.com'),
(7, 'test7', 'pass7', 'test7@example.com'),
(8, 'test8', 'pass8', 'test8@example.com'),
(9, 'test9', 'pass9', 'test9@example.com'),
(10, 'test10', 'pass10', 'test10@example.com'),
(11, 'test11', 'pass11', 'test11@example.com'),
(12, 'test12', 'pass12', 'test12@example.com'),
(13, 'test13', 'pass13', 'test13@example.com'),
(14, 'test14', 'pass14', 'test14@example.com'),
(15, 'test15', 'pass15', 'test15@example.com'),
(16, 'test16', 'pass16', 'test16@example.com'),
(17, 'test17', 'pass17', 'test17@example.com'),
(18, 'test18', 'pass18', 'test18@example.com'),
(19, 'test19', 'pass19', 'test19@example.com'),
(20, 'test20', 'pass20', 'test20@example.com'),
(21, 'test21', 'pass21', 'test21@example.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transmission`
--

DROP TABLE IF EXISTS `transmission`;
CREATE TABLE IF NOT EXISTS `transmission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
