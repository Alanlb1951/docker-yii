<?php

interface Handler {
    function execute();
}