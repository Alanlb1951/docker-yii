<?php

class ReadService {

    function __invoke( $id = null ) {
       $dataResult = Yii::app()->db->createCommand()
            ->select('*')
            ->from('broker');

        if( !empty( $id ) && is_numeric( $id ) )
            return $dataResult->where( 'id = :id', [
                ':id' => $id
            ])->queryRow(); 

        return  $dataResult->queryAll();
    }

}