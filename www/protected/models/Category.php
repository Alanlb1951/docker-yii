<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $category_id
 * @property string $category_name_es
 * @property string $category_name_en
 * @property integer $category_active
 * @property string $category_deductible
 * @property string $category_autorization
 * @property string $category_code
 * @property string $category_order
 */
class Category extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_name_es, category_name_en', 'required'),
			array('category_active', 'numerical', 'integerOnly'=>true),
			array('category_name_es, category_name_en, category_order', 'length', 'max'=>45),
			array('category_deductible, category_autorization', 'length', 'max'=>10),
			array('category_code', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('category_id, category_name_es, category_name_en, category_active, category_deductible, category_autorization, category_code, category_order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'category_id' => 'Category',
			'category_name_es' => 'Category Name Es',
			'category_name_en' => 'Category Name En',
			'category_active' => 'Category Active',
			'category_deductible' => 'Category Deductible',
			'category_autorization' => 'Category Autorization',
			'category_code' => 'Category Code',
			'category_order' => 'Category Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('category_name_es',$this->category_name_es,true);
		$criteria->compare('category_name_en',$this->category_name_en,true);
		$criteria->compare('category_active',$this->category_active);
		$criteria->compare('category_deductible',$this->category_deductible,true);
		$criteria->compare('category_autorization',$this->category_autorization,true);
		$criteria->compare('category_code',$this->category_code,true);
		$criteria->compare('category_order',$this->category_order,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
