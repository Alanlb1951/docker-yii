<?php

/**
 * This is the model class for table "fleet".
 *
 * The followings are the available columns in table 'fleet':
 * @property integer $id
 * @property string $acriss
 * @property string $description
 * @property integer $doors
 * @property integer $passangers
 * @property integer $luggages
 * @property integer $category_id
 * @property integer $type_id
 * @property integer $transmission_id
 */
class Fleets extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fleet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('acriss, description, doors, passangers, luggages, category_id, type_id, transmission_id', 'required'),
			array('doors, passangers, luggages, category_id, type_id, transmission_id', 'numerical', 'integerOnly'=>true),
			array('acriss', 'length', 'max'=>10),
			array('description', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, acriss, description, doors, passangers, luggages, category_id, type_id, transmission_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'acriss' => 'Acriss',
			'description' => 'Description',
			'doors' => 'Doors',
			'passangers' => 'Passangers',
			'luggages' => 'Luggages',
			'category_id' => 'Category',
			'type_id' => 'Type',
			'transmission_id' => 'Transmission',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('acriss',$this->acriss,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('doors',$this->doors);
		$criteria->compare('passangers',$this->passangers);
		$criteria->compare('luggages',$this->luggages);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('type_id',$this->type_id);
		$criteria->compare('transmission_id',$this->transmission_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fleets the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
