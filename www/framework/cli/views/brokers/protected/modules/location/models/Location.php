<?php

/**
 * This is the model class for table "location".
 *
 * The followings are the available columns in table 'location':
 * @property integer $id
 * @property string $iata
 * @property string $name
 * @property string $address_1
 * @property string $address_2
 * @property integer $is24hours
 * @property string $open_schedule
 * @property string $close_schedule
 * @property string $latitude
 * @property string $longitude
 * @property string $cp
 * @property integer $city_id
 * @property integer $provider_id
 * @property integer $broker_id
 */
class Location extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('iata, name, address_1, address_2, is24hours, open_schedule, close_schedule, latitude, longitude, cp, city_id, provider_id, broker_id', 'required'),
			array('is24hours, city_id, provider_id, broker_id', 'numerical', 'integerOnly'=>true),
			array('iata, open_schedule, close_schedule', 'length', 'max'=>10),
			array('name, address_1, address_2', 'length', 'max'=>150),
			array('latitude, longitude', 'length', 'max'=>50),
			array('cp', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, iata, name, address_1, address_2, is24hours, open_schedule, close_schedule, latitude, longitude, cp, city_id, provider_id, broker_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'iata' => 'Iata',
			'name' => 'Name',
			'address_1' => 'Address 1',
			'address_2' => 'Address 2',
			'is24hours' => 'Is24hours',
			'open_schedule' => 'Open Schedule',
			'close_schedule' => 'Close Schedule',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'cp' => 'Cp',
			'city_id' => 'City',
			'provider_id' => 'Provider',
			'broker_id' => 'Broker',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('iata',$this->iata,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address_1',$this->address_1,true);
		$criteria->compare('address_2',$this->address_2,true);
		$criteria->compare('is24hours',$this->is24hours);
		$criteria->compare('open_schedule',$this->open_schedule,true);
		$criteria->compare('close_schedule',$this->close_schedule,true);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('cp',$this->cp,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('provider_id',$this->provider_id);
		$criteria->compare('broker_id',$this->broker_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Location the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
