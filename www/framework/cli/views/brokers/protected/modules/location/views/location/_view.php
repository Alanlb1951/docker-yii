<?php
/* @var $this LocationController */
/* @var $data Location */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iata')); ?>:</b>
	<?php echo CHtml::encode($data->iata); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_1')); ?>:</b>
	<?php echo CHtml::encode($data->address_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_2')); ?>:</b>
	<?php echo CHtml::encode($data->address_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is24hours')); ?>:</b>
	<?php echo CHtml::encode($data->is24hours); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('open_schedule')); ?>:</b>
	<?php echo CHtml::encode($data->open_schedule); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('close_schedule')); ?>:</b>
	<?php echo CHtml::encode($data->close_schedule); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitude')); ?>:</b>
	<?php echo CHtml::encode($data->latitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('longitude')); ?>:</b>
	<?php echo CHtml::encode($data->longitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cp')); ?>:</b>
	<?php echo CHtml::encode($data->cp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city_id')); ?>:</b>
	<?php echo CHtml::encode($data->city_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('provider_id')); ?>:</b>
	<?php echo CHtml::encode($data->provider_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('broker_id')); ?>:</b>
	<?php echo CHtml::encode($data->broker_id); ?>
	<br />

	*/ ?>

</div>