# Servidor LAMP con aplicación Yii en Docker
Este repositorio contiene un archivo docker-compose.yml que describe un servidor LAMP (Linux, Apache, MySQL, PHP) en Docker que contiene una aplicación Yii versión 1.1.

## Servicios
El archivo define tres servicios:

## db
Este servicio crea un contenedor para la base de datos MySQL. Se especifican puertos, variables de entorno para establecer las credenciales de la base de datos, volúmenes para persistir los datos de la base de datos fuera del contenedor y la red a la que pertenece el contenedor.

## www
Este servicio crea un contenedor para el servidor web Apache y PHP que ejecutará la aplicación Yii. Se especifican los puertos, los volúmenes que montarán el código de la aplicación en el contenedor y los links a otros servicios, en este caso, el servicio db.

## phpmyadmin
Este servicio crea un contenedor para PhpMyAdmin, una herramienta web que permite administrar la base de datos MySQL. Se especifican los puertos, los links a otros servicios y las variables de entorno para establecer las credenciales de acceso a la base de datos.

## Volúmenes
Además, se define un volumen persistente que se utiliza para persistir los datos de la base de datos fuera de los contenedores.

Ejecución
Para levantar los contenedores y ejecutar la aplicación, ejecute el comando docker-compose up en el directorio donde se encuentra el archivo docker-compose.yml. La aplicación Yii estará disponible en http://localhost y PhpMyAdmin en http://localhost:8000.